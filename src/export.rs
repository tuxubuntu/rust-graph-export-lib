extern crate rmp;
extern crate serde;
extern crate serde_json;


use self::serde::{
	Deserialize,
	Serialize
};

use super:: {
	ERR_CANT_IMPORT,
	ERR_CANT_EXPORT
};

use super::graph_lib::{
	Graph,
	Payloaded,
	Initiated,
	GraphBasic,
	errors:: { Result }
};

type Enc<T> = (u8, Vec<(usize, usize)>, Vec<Option<T>>);


#[derive(Debug, PartialEq, Serialize, Deserialize)]
struct Serialized<T: Clone+Serialize>(Enc<T>);

#[derive(Debug)]
pub struct Exporter;

impl Exporter {
	fn to_struct<I, T: Clone, G: Graph<G, I>+Payloaded<T, G, I>>(graph: &G) -> Enc<T> {
		let mut acc = (1, Vec::new(), Vec::new());
		fn helper<I, T: Clone, G: Graph<G, I>+Payloaded<T, G, I>>(acc: &mut Enc<T>, graph: &G) {
			let payload = match graph.payload_get() {
				Some(payload) => Some(payload.clone()),
				None => None,
			};
			let id = acc.1.len();
			acc.2.push(payload);
			for child in graph.children() {
				acc.1.push((id, acc.2.len()));
				helper(acc, child);
			}
		}
		helper(&mut acc, graph);
		acc
	}
	fn from_struct<T: Clone>(source: Enc<T>) -> Option<GraphBasic<T>> {
		fn helper<T: Clone>(cursor: usize, source: &Enc<T>) -> Option<GraphBasic<T>> {
			let (_, rels, contents) = source;
			if let Some(val) = contents.get(cursor) {
				let mut graph: GraphBasic<T> = GraphBasic::new();
				if let Some(val) = val {
					graph.payload_set(val.clone());
				}
				for (a, b) in rels {
					if a.clone() == cursor {
						if let Some(child) = helper(b.clone(), source) {
							graph.add(child);
						}
					}
				}
				Some(graph)
			} else {
				None
			}
		}
		helper(0, &source)
	}
	pub fn import<'de, T: Clone+Serialize+Deserialize<'de>>(buf: &'de Vec<u8>) -> Result<GraphBasic<T>> {
		let a: &'de [u8] = buf.as_slice();
		let Serialized(source): Serialized<T> = serde_json::from_slice(a).unwrap();
		match Exporter::from_struct(source) {
			Some(graph) => Ok(graph),
			None => Err(ERR_CANT_IMPORT),
		}
	}
	pub fn export<'de, I, T: Clone+Serialize+Deserialize<'de>, G: Graph<G, I>+Payloaded<T, G, I>>(graph: &G) -> Result<Vec<u8>> {
		let mut buf = Vec::new();
		if let Err(_) = rmp::encode::write_bool(&mut buf, true) {
			return Err(ERR_CANT_EXPORT)
		}
		let source = Exporter::to_struct(graph);
		let val = Serialized(source);

		let serialized = serde_json::to_string(&val).unwrap();
		Ok(Vec::from(serialized.as_bytes()))
	}
}
