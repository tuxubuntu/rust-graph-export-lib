
use super::graph_lib::{
	Graph,
	Initiated,
	Payloaded,
	GraphBasic
};

#[test]
fn export_import() {
	use *;
	let mut graph = GraphBasic::new();
	let n0 = graph.make();
	let n1 = graph.get_mut(&n0).unwrap().make();
	graph.get_mut(&n0).unwrap().get_mut(&n1).unwrap().payload_set(true);
	let blob = Exporter::export(&graph).unwrap();
	// println!("{:?}", String::from_utf8(blob).unwrap());
	// "[1,[[0,1],[1,2]],[null,null,true]]"
	let graph1: GraphBasic<bool> = Exporter::import(&blob).unwrap();
	assert_eq!(*graph1.get(&n0).unwrap().get(&n1).unwrap().payload_get().unwrap(), true);
}
